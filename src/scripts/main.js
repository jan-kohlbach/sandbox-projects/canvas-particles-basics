(() => {
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');
  document.body.appendChild(canvas);

  const resize = () => {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight / 2;
  };

  resize();

  window.addEventListener('resize', resize);

  context.fillStyle = 'rgb(34, 34, 34)';
  context.fillRect(0, 0, canvas.width, canvas.height);

  const particles = [];

  const draw = (particle) => {
    const element = particle;
    element.x += particle.vx;
    element.y -= particle.vy;

    context.beginPath();
    context.fillStyle = '#ddd';
    context.arc(particle.x, particle.y, particle.size, 0, Math.PI * 2, true);
    context.closePath();
    context.fill();
  };

  const generate = () => {
    const particle = {
      x: (canvas.width / 2) + Math.random() * 200 - 100,
      y: (canvas.height / 2) + Math.random() * 200 - 100,
      size: 2,
      vx: Math.random() * 4 - 2,
      vy: Math.random() * 4 - 2,
    };

    particles.push(particle);

    if (particles.length > 300) {
      particles.shift();
    }

    context.fillStyle = 'rgba(34, 34, 34, 1)';
    context.fillRect(0, 0, canvas.width, canvas.height);

    for (let i = 0; i < particles.length; i += 1) {
      const p = particles[i];
      draw(p);
    }

    window.requestAnimationFrame(generate);
  };

  window.requestAnimationFrame(generate);


  const canvas2 = document.createElement('canvas');
  const context2 = canvas2.getContext('2d');
  document.body.appendChild(canvas2);

  context2.fillStyle = 'rgb(34, 34, 34)';
  context2.fillRect(0, 0, canvas2.width, canvas2.height);

  let posX;
  let posY;
  let vx;
  let vy;
  const gravity = 1;
  const size = 10;

  const resize2 = () => {
    canvas2.width = window.innerWidth;
    canvas2.height = window.innerHeight / 2;
    posX = canvas2.width / 5;
    posY = canvas2.height / 2;
    vx = window.innerWidth / 100;
    vy = -window.innerHeight / 100;
  };

  resize2();

  window.addEventListener('resize', resize2);

  const drawParticle = () => {
    posX += vx;
    posY += vy;

    if (posY > canvas2.height * 0.8) {
      vy *= -0.6;
      vx *= 0.6;
      posY = canvas2.height * 0.8;
    }

    vy += gravity;

    context2.fillStyle = 'rgba(34, 34, 34, .1)';
    context2.fillRect(0, 0, canvas2.width, canvas2.height);

    context2.beginPath();
    context2.fillStyle = '#ddd';
    context2.arc(posX, posY, size, 0, Math.PI * 2, true);
    context2.closePath();
    context2.fill();

    window.requestAnimationFrame(drawParticle);
  };

  window.requestAnimationFrame(drawParticle);
})();
